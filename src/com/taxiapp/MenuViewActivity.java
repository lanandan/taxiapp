package com.taxiapp;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.taxiapp.fragment.MenuDetailFragment;
import com.taxiapp.fragment.ProfileFragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MenuViewActivity extends Activity {
	String[] menu;
	DrawerLayout dLayout;
	ListView dList;
	ArrayAdapter<String> adapter;
	private GoogleMap googleMap;

	ImageView menu_btn;
	ListView drawer_lst;

	TextView txt_address;

	double lat, longt;

	ImageView imgvwMenu, img_searching;
	private DrawerLayout mDrawerLayout;
	private ActionBarDrawerToggle mDrawerToggle;
	private ListView mDrawerList;
	private String[] mSlideMenuList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu);

		txt_address = (TextView) findViewById(R.id.adds);
		img_searching=(ImageView)findViewById(R.id.img_search);
		
		img_searching.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				initilizeMap();
				findlocation();
			}
		});

		imgvwMenu = (ImageView) findViewById(R.id.imgvw_menu);
		mDrawerLayout = (DrawerLayout) findViewById(R.id.slidermenu_main);
		mSlideMenuList = getResources().getStringArray(R.array.menu_array);

		imgvwMenu.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (!mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
					mDrawerLayout.openDrawer(Gravity.LEFT);
					mDrawerLayout.bringToFront();
				} else {
					mDrawerLayout.closeDrawer(Gravity.LEFT);
				}

			}
		});

		mDrawerList = (ListView) findViewById(R.id.left_drawer);
		mDrawerList.setAdapter(new ArrayAdapter<String>(MenuViewActivity.this,
				R.layout.drawer_list_item, mSlideMenuList));
		mDrawerList
				.setOnItemClickListener(new AdapterView.OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						if (position == 0) {
							mDrawerLayout.closeDrawer(Gravity.LEFT);
							Intent first=new Intent(MenuViewActivity.this,MenuViewActivity.class);
							startActivity(first);
							overridePendingTransition(R.anim.right_in, R.anim.left_out);
						}
						if (position == 1) {
							mDrawerLayout.closeDrawer(Gravity.LEFT);
						
							Intent first=new Intent(MenuViewActivity.this,ProfileActivity.class);
							startActivity(first);
							
							overridePendingTransition(R.anim.right_in, R.anim.left_out);

						}
						if (position == 2) {
							mDrawerLayout.closeDrawer(Gravity.LEFT);
							/*FragmentManager fragmentManager = getFragmentManager();
							FragmentTransaction fragmentTransaction = fragmentManager
									.beginTransaction();
							ProfileFragment fragment = new ProfileFragment();
							fragmentTransaction.replace(R.id.frame_container,
									fragment);
							fragmentTransaction.commit();*/
							overridePendingTransition(R.anim.right_in, R.anim.left_out);

						}
						if (position == 3) {
							mDrawerLayout.closeDrawer(Gravity.LEFT);
							overridePendingTransition(R.anim.right_in, R.anim.left_out);

						}
						if (position == 4) {
							mDrawerLayout.closeDrawer(Gravity.LEFT);

						}
						if (position == 5) {
							mDrawerLayout.closeDrawer(Gravity.LEFT);

						}
						if (position == 6) {
							mDrawerLayout.closeDrawer(Gravity.LEFT);

						}
						

					}
				});

		try {
			// Loading map

			initilizeMap();
			findlocation();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void initilizeMap() {
		// TODO Auto-generated method stub
		if (googleMap == null) {
			googleMap = ((MapFragment) getFragmentManager().findFragmentById(
					R.id.map1)).getMap();
			findlocation();
			// latitude and longitude
			double latitude = lat;
			double longitude = longt;

			// create marker
			MarkerOptions marker = new MarkerOptions().position(
					new LatLng(latitude, longitude)).title("Current Location");
			// Changing marker icon
			marker.icon(BitmapDescriptorFactory
					.fromResource(R.drawable.car_taxi));

			// adding marker
			googleMap.addMarker(marker);

			CameraPosition cameraPosition = new CameraPosition.Builder()
					.target(new LatLng(lat, longt)) // Sets thet center of the
					.zoom(17) // Sets the zoom
					.bearing(90) // Sets the orientation of the camera to east
					.tilt(40) // Sets the tilt of the camera to 30 degrees
					.build(); // Creates a CameraPosition from the builder
			googleMap.animateCamera(CameraUpdateFactory
					.newCameraPosition(cameraPosition));

			// check if map is created successfully or not
			if (googleMap == null) {
				Toast.makeText(getApplicationContext(),
						"Sorry! unable to create maps", Toast.LENGTH_SHORT)
						.show();
			}
		}
	}

	public void findlocation() {
		GPSTracker gpsTracker = new GPSTracker(this);
		if (gpsTracker.getIsGPSTrackingEnabled()) {
			String stringLatitude = String.valueOf(gpsTracker.latitude);
			lat = gpsTracker.latitude;
			String stringLongitude = String.valueOf(gpsTracker.longitude);
			longt = gpsTracker.longitude;
			String country = gpsTracker.getCountryName(this);
			String city = gpsTracker.getLocality(this);
			String postalCode = gpsTracker.getPostalCode(this);
			String addressLine = gpsTracker.getAddressLine(this);
			txt_address.setText(addressLine + "," + city + "," + country + ","
					+ postalCode);

		} else {

			gpsTracker.showSettingsAlert();
		}

	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.imgvw_menu:
			if (!mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
				mDrawerLayout.openDrawer(Gravity.LEFT);
				mDrawerLayout.bringToFront();
			} else {
				mDrawerLayout.closeDrawer(Gravity.LEFT);
			}
			break;
		}
	}

}
