package com.taxiapp;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import android.app.Activity;

import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;

import android.view.View;

import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MapActivity extends Activity {

	private GoogleMap googleMap;

	ImageView menu_btn;
	ListView drawer_lst;

	TextView txt_address;

	double lat, longt;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);
		menu_btn = (ImageView) findViewById(R.id.menu_btn);
		// drawer_lst = (ListView) findViewById(R.id.left_drawer);
		txt_address = (TextView) findViewById(R.id.add);

		menu_btn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// drawer_lst.bringChildToFront(drawer_lst);

			}
		});

		try {
			// Loading map
			initilizeMap();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void initilizeMap() {
		// TODO Auto-generated method stub
		if (googleMap == null) {
			googleMap = ((MapFragment) getFragmentManager().findFragmentById(
					R.id.map)).getMap();
			findlocation();
			// latitude and longitude
			double latitude = lat;
			double longitude = longt;

			// create marker
			MarkerOptions marker = new MarkerOptions().position(
					new LatLng(latitude, longitude)).title("Current Location");
			// Changing marker icon
			marker.icon(BitmapDescriptorFactory
					.fromResource(R.drawable.car_taxi));

			// adding marker
			googleMap.addMarker(marker);

			CameraPosition cameraPosition = new CameraPosition.Builder()
					.target(new LatLng(lat, longt)) // Sets thet center of the
					.zoom(17) // Sets the zoom
					.bearing(90) // Sets the orientation of the camera to east
					.tilt(40) // Sets the tilt of the camera to 30 degrees
					.build(); // Creates a CameraPosition from the builder
			googleMap.animateCamera(CameraUpdateFactory
					.newCameraPosition(cameraPosition));

			// check if map is created successfully or not
			if (googleMap == null) {
				Toast.makeText(getApplicationContext(),
						"Sorry! unable to create maps", Toast.LENGTH_SHORT)
						.show();
			}
		}
	}

	public void findlocation() {
		GPSTracker gpsTracker = new GPSTracker(this);
		if (gpsTracker.getIsGPSTrackingEnabled()) {
			String stringLatitude = String.valueOf(gpsTracker.latitude);
			lat = gpsTracker.latitude;
			String stringLongitude = String.valueOf(gpsTracker.longitude);
			longt = gpsTracker.longitude;
			String country = gpsTracker.getCountryName(this);
			String city = gpsTracker.getLocality(this);
			String postalCode = gpsTracker.getPostalCode(this);
			String addressLine = gpsTracker.getAddressLine(this);
			txt_address.setText(addressLine + "," + city + "," + country + ","
					+ postalCode);

		} else {

			gpsTracker.showSettingsAlert();
		}

	}

}
