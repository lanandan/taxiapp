package com.taxiapp;

import android.app.Activity;
import android.os.Bundle;

public class ForgetPasswordActivity extends Activity{
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.forgetpassword);
	}
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.animation_enter,
				R.anim.animation_leave);

	}
}
