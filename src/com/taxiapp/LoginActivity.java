package com.taxiapp;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class LoginActivity extends Activity {

	View v;

	Button btn_login;
	TextView txt_createnew,txt_forget;
	
	Intent subActivity = new Intent();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		btn_login = (Button) findViewById(R.id.btn_login);
		txt_createnew = (TextView) findViewById(R.id.txt_create);
		txt_forget=(TextView)findViewById(R.id.txt_forget);

		btn_login.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				subActivity.setClass(LoginActivity.this, MenuViewActivity.class);
				startActivity(subActivity);
				overridePendingTransition(R.anim.right_in, R.anim.left_out);
			}
		});

		txt_createnew.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				subActivity.setClass(LoginActivity.this, RegisterActivity.class);
				startActivity(subActivity);
				overridePendingTransition(R.anim.right_in, R.anim.left_out);
			}
		});
		
		
		
		txt_forget.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				subActivity
						.setClass(LoginActivity.this, ForgetPasswordActivity.class);
				startActivity(subActivity);
				overridePendingTransition(R.anim.right_in, R.anim.left_out);
			}
		});
		
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.animation_enter,
				R.anim.animation_leave);

	}
}
