package com.taxiapp;

import android.app.Activity;
import android.os.Bundle;

public class HomeActivity extends Activity {

	protected void OnCreated(Bundle SavedData) {
		super.onCreate(SavedData);
		setContentView(R.layout.activity_home);
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.animation_enter,
				R.anim.animation_leave);

	}
}
