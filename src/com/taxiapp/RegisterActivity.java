package com.taxiapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class RegisterActivity extends Activity {

	View v;
	Intent register=new Intent();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
	}
	
	
	
	public void regClick(View v){
		
		switch (v.getId()) {
		case R.id.btn_reg:
			register.setClass(RegisterActivity.this, ForgetPasswordActivity.class);
			startActivity(register);
			break;

		default:
			break;
		}
		
		overridePendingTransition(R.anim.right_in, R.anim.left_out);
		
	}
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.animation_enter,
				R.anim.animation_leave);

	}
}
